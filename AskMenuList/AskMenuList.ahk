﻿
#Requires AutoHotkey >=v2.0-

; THIS IS TESTING/EXAMPLE CODE, COMMENT IT WHEN HE IS USELESS

; #SingleInstance Force
; #Warn All, MsgBox

; ; Build your matrix choice
; matrixChoice := [
;   ['&Txt displayed', 'Firts choice Value'],
;   ['Second choice `t Right side information', 2],
;   ,                                   ; <-- empty value Add a delimiter
;   ['&Sub Menu`thit S',  [             ; <-- sub matrix for sub menu
;       ['>Checked option begin with >', 'Sub Menu - Firts choice Value'],
;       ['>*Checked Radio option begin with >*', 5],
;       ['*UnChecked Radio option begin with *', 6],
;       ['return 22', 22],
;       ['&Last option`thit L', 'Sub Menu - Value returned by last option']
;     ]
;   ],
;   ['&Sub Menu 2',  [         
;       ['i have no inspiration', 'Sub Menu 2 - Firts choice Value'],
;       ['&Another subMenu `t Hit A',  [   ; <-- sub sub matrix for sub sub menu...
;           ['Ok, you find me !', 'Farest value']
; ]]]]]

; ; Ask to the user NOW and wait an answer. the value 'Nevermind =P' is the value return in case of no choice from user
; valChosen := askMenuList(matrixChoice, 'Nevermind =P')

; msgBox 'You chose <' valChosen '>'

; --------------- END TEST/EXAMPLE CODE -------------------------------


Global askMenuList_returnVal

askMenuList(matChoice_diplay_value, noChoice := '')
{
  global askMenuList_returnVal := noChoice

  askMenuList_buildMenu(matChoice_diplay_value).show() ; <-- hope reference on  Menu object will not be lost and object will be delete after the show() =P maybe I have to store him in local variable
  
  return askMenuList_returnVal
}

askMenuList_buildMenu(matMenu)
{
  _menu := Menu()
  for key, display_value in matMenu
  {
    if(IsSet(display_value) and (not(display_value is Array) or display_value.Length))
    {
      if display_value is Array
      {
        display := display_value[1]
        value   := display_value[2]
      }
      else
      {
        display := display_value
        value   := strReplace(LTrim(display, '>*'), '&')
      }

      chkChr := SubStr(display, 1, 2)
      
      if inStr(chkChr, '*')
        option := '+Radio'
      else
        option := ''

      display := LTrim(display, '>*')

      if(value is Array) ; if value is an array ([Name, [...]]), this is a sub-menu
        _menu.Add(display, askMenuList_buildMenu(value))
      else
      {                      
        _menu.Add(display, askMenuList_select.Bind(value), option)
        if inStr(chkChr, '>')
          _menu.Check(display)
      }
    }
    else ; <-- else if empty value (no pair [Name, Value]), we add a separator
      _menu.Add()
  }
  return _menu
}

askMenuList_select(value, *)
{
  global askMenuList_returnVal := value
}
