# Windows Explorer Tools

Adds functionality with :
 - Clipboard (Ctrl+V) 
 - Windows Explorer
 - Interaction between a text and Windows (specially windows path)


Quickly, you will be able to :
 - Past the file/folder path from clipboard's files
 - Open a file or its parent folder from a path, anyware, anytime
 - Copy one or more files from a path
 - Past the clipboard content directy in a new file (txt, picture)
 - Get PID of any window, open his install folder or make it allways on top
 - Kill a window without the task manager
 - Set a window allways on Top
 - Invert '\\' and '/' inside a path, or escape all '\\' in '\\\\'
 - Create Hypertext link when you past an URL 


<!-- ## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method. -->

## Installation
[Download WindowsExplorerTools.exe](https://gitlab.com/nitrateag/windowsexplorertools/-/releases), put it in an empty directory (or replace an older version to update) and double click, it's ready !

## Usage
Right click on the tray icon (generaly the little icon in down-right of your screen) and click "[help - EN](https://gitlab.com/nitrateag/windowsexplorertools/-/blob/main/WindowsExplorerTools/Help/HelpEn.md)" or "[help - FR](https://gitlab.com/nitrateag/windowsexplorertools/-/blob/main/WindowsExplorerTools/Help/HelpFr.md)"
to see all functionality.


## License
GNU GENERAL PUBLIC LICENSE
Version 3, 29 June 2007
