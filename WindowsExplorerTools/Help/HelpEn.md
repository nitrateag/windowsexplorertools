# WindowsExplorerTools <!-- omit in toc -->

WindowsExplorerTools is a contextual toolbox that aims to simplify user interaction with the Windows environment using keyboard shortcuts.

   

- [Ctrl+V (+Alt/+Maj) :](#ctrlv-altmaj-)
  - [In Windows Explorer:](#in-windows-explorer)
  - [In the address bar of Windows file explorer:](#in-the-address-bar-of-windows-file-explorer)
  - [In any other text area:](#in-any-other-text-area)
- [Win+Enter (+Alt/+Maj) :](#winenter-altmaj-)
  - [On a selection of *text* or a *Windows control*:](#on-a-selection-of-text-or-a-windows-control)
  - [Ctrl+Win+Enter (+Alt/+Maj):](#ctrlwinenter-altmaj)
- [Win+/ on a text selection:](#win-on-a-text-selection)
- [Win+F2 on a selection of files in Windows Explorer :](#winf2-on-a-selection-of-files-in-windows-explorer-)
- [Win+RightClick on any software :](#winrightclick-on-any-software-)
- [Win+Alt+Up on any software :](#winaltup-on-any-software-)
- [VirtualBox Compatibility:](#virtualbox-compatibility)


&nbsp;

---
## Ctrl+V (+Alt/+Maj) :
### In Windows Explorer:
  - If the Clipboard contains text with one or more Windows path(s):
    - Copy each file/folder in the clipboard to the current explorer.
    - Paths can contain wildcards like 'C:/folder/*.txt' to select multiple files in a single path.
  - **Variation +Shift (Ctrl+Shift+V):**
    - If the clipboard contains text or an image, a window will prompt you to save the clipboard's content.
  
### In the address bar of Windows file explorer:
  - If the clipboard contains a file or path to a file:
    - Paste the path to the folder containing the file.
  - If there are multiple paths/files/folders in the clipboard:
    - You will be prompted to select which paths to paste.
  - If the file or folder path is invalid:
    - The first valid parent will be pasted.

### In any other text area:
  - If the clipboard contains files (copied from a file explorer):
    - If only one file is copied, the path is pasted without quotes.
    - If multiple files are copied, the pasted paths are delimited by quotes and separated by a space. It is optimized to be pasted in a file open window.
    - **Variation +Shift (Ctrl+Shift+V):**
      - Forces quotes on paths copied from explorer, even if there is only one copied path.
      - If there are multiple files, a window will prompt you to select which character(s) to use to separate the paths.
    - **Variation +Alt (Ctrl+Alt+V):**
      - Forces default paste behavior, without any particular behavior.
  - If the clipboard is exactly one URL:
    - An HTML hypertext will be generated before the paste. If your software support HTML5 (ex : Discord, Teams...) or support page layout (ex : Microsoft Office suite ...), a special link "HomePage/specificPage" will be paste. (Else, the URL will be paste as usual txt)
    - **Variation +Shift (Ctrl+Shift+V):**
      - The URL will be paste as usual txt
  - If the clipboard contains text:
    - The text from the clipboard is pasted normally, without any particular behavior.
    - **Variation +Shift (Ctrl+Shift+V):**
      - Searches the clipboard for Windows paths and pastes only the Windows paths.
      - If the clipboard does not contain Windows paths but the text is copied with a specific font, only the plain text is pasted.

---
## Win+Enter (+Alt/+Maj) :
Win+Enter has the main function of executing Windows paths. Whether it's opening the path in Windows Explorer, opening the file with its default software, or executing a path to an executable and its arguments.

### On a selection of *text* or a *Windows control*:
*Understand "Windows control" as a pop-up displaying an error message that contains a Windows path. This extends to any UI object that allows you to do a Ctrl+C to retrieve a message, such as doing a Ctrl+C on an compilator error line.*

  - If the text (*an error message, a warning...*) contains a path to a folder/file/executable:
    - We open the file with its default software, execute the exe, or open a Windows explorer at the selected path.
    ***=> You can select your path roughly! <=
    => We detect the presence of the path in any text <=***
  - If multiple paths are found:
    - You are asked which path you want to open (a multiple choice menu appears).
  - **Variant +Alt (Win+Alt+Enter):**
     - Opens the parent folder of the selected path.
  - **Variant +Maj (Win+Maj+Enter):**
    - Executes the selected text without searching for paths in the text selection:
      *=> You must apply yourself to the text selection for this variant.<=*
    - Useful if you are executing text that has no connection with a Windows path, such as:
      - Opening an https link.
      - An .exe WITH parameters.
    - Useful if the path you want to open is too exotic to be recognized (ex: there are commas in a folder or file name!)

### Ctrl+Win+Enter (+Alt/+Maj):
*Similar to Win+Enter, but this time we retrieve paths from the current clipboard.
Useful if the text cannot be retrieved by a simple Ctrl+C, such as Planet error messages: you first have to right-click>Copy to retrieve the message*
  - If the clipboard contains a path to a folder/file/executable:
    - We open the file with its default software, execute the exe, or open a Windows explorer at the selected path.
    ***=> We detect the presence of the path in any text of the clipboard<=***
  - If multiple paths are found:
    - You are asked which path you want to open (a multiple choice menu appears).
  - **Variant +Alt (Ctrl+Win+Alt+Enter):**
     - Opens the parent folder of the found path.
  - **Variant +Maj (Ctrl+Win+Maj+Enter):**
    - Executes the text contained in the clipboard without searching for paths in the text selection:
      *=> You must apply yourself to the text selection for this variant.<=*
    - Useful if you are executing text that has no connection with a Windows path, such as:
      - Opening an https link.
      - An .exe WITH parameters.
    - Useful if the path you want to open is too exotic to be recognized (ex: there are commas in a folder or file name!)

---
## Win+/ on a text selection:
  - Offers replacing '/' with '\\'.
  - Offers escaping '\\' as '\\\\'.
  - Offers replacing '\\' and '\\\\' with '/'.


---

## Win+F2 on a selection of files in Windows Explorer :
 - Adds a prefix to the names of the selected files.

---

## Win+RightClick on any software :
Open a multiple choice menu:
  - Offers to keep the window always on top
  - Offers to open the installation folder of the application.
  - Offers to copy the path to the window's executable.
  - Offers to copy the PID of the window.
  - Offers to kill the application pointed by the mouse (a pop-up asks you to confirm the action).

## Win+Alt+Up on any software :
Set or unset "Always on top" on the active window


&nbsp;

---

## VirtualBox Compatibility:

It is possible to translate paths from the host machine to the virtual machine under certain conditions.
- **Requires:**
    - Installed "Guest Additions".
    - Enabled bidirectional clipboard.
    - Mounted a shared folder pointing to the root of your host machine disks.
      - Left the default name of the mount (so that 'C:\' appears in the VM under \\\\Vboxsvr\c_drive).
    - Launched the "ExplorateurTools.exe" macro in the VM.
- **Then, you can:**
    - Paste files Host --> VM directly (Ctrl+V only, not via the context menu)
      - The contrapositive VM --> Host does not work (it is not possible to access a file on the VM disk from the host machine =(
    - If you copy text from the host machine --> VM, and it contains paths, these paths are automatically translated into paths compatible with the VM (when the text is pasted into the VM).
      - The contrapositive VM --> Host is "***possible***" as long as the paths point to a shared folder of the VM.
      - Very useful for converting scripts calling files on the host machine:
        - Ctrl+A, Ctrl+C in the script of the host machine, Ctrl+V in the script of the VM: All paths from the host machine will be translated into paths compatible with the VM.

