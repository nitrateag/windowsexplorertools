﻿***
# WindowsExplorerTools <!-- omit in toc -->

WindowsExplorerTools est une boite à outils contextuel qui vise à simplifier l'interaction de l'utilisateur avec l'environnement Windows à l'aide de raccourcis clavier.

   

- [Ctrl+V (+Alt/+Maj) :](#ctrlv-altmaj-)
  - [Dans l'explorateur Windows :](#dans-lexplorateur-windows-)
  - [Dans la barre d'adresse de l'explorateur de fichier Windows :](#dans-la-barre-dadresse-de-lexplorateur-de-fichier-windows-)
  - [Dans n'importe quel autre zone de texte :](#dans-nimporte-quel-autre-zone-de-texte-)
- [Win+Enter (+Alt/+Maj) :](#winenter-altmaj-)
  - [Sur une sélection de *texte* ou un *control Windows* :](#sur-une-sélection-de-texte-ou-un-control-windows-)
  - [Ctrl+Win+Enter (+Alt/+Maj) :](#ctrlwinenter-altmaj-)
- [Win+/ sur une sélection de texte :](#win-sur-une-sélection-de-texte-)
- [Win+F2 sur une sélection de fichiers de l'explorateur Windows :](#winf2-sur-une-sélection-de-fichiers-de-lexplorateur-windows-)
- [Win+ClickDroit sur n'importe quel fenêtre Windows :](#winclickdroit-sur-nimporte-quel-fenêtre-windows-)
- [Win+Alt+Up sur n'importe quel fenêtre Windows :](#winaltup-sur-nimporte-quel-fenêtre-windows-)
- [Compatibilité VirtualBox :](#compatibilité-virtualbox-)


&nbsp;

---
## Ctrl+V (+Alt/+Maj) :
### Dans l'explorateur Windows :
  - Si le Clipboard contient du text avec un ou plusieur path(s) Windows :
    - Copie chaque fichiers/dossiers du clipboard dans l'explorateur courant.
    - Les paths peuvent contenir des wildcard type 'C:/dossier/*.txt' pour selectionner plusieur fichiers en un seul path.
  - **Variante +Maj (Ctrl+Maj+V)** :
    - Si le Clipboard contient du text ou une image, une fenetre vous propose d'enregistrer le contenu du clipboard
  
### Dans la barre d'adresse de l'explorateur de fichier Windows :
  - Si le clipoard contient un fichier ou un path vers un fichier
    - Colle le chemin vers le dossier contenant le fichier.
  - S'il y a plusieur paths/fichier/dossier dans le clipboard :
    - On vous demande de selectionner quel paths vous voulez coller.
  - Si le chemin vers le fichier ou le dossier est invalide :
    - Le premier parent valide sera collé

### Dans n'importe quel autre zone de texte :
  - Si le clipboard contient des fichiers (copiés depuis un explorateur de fichier) :
    - S'il n'y a qu'un seul fichier copié, le path est collé sans guillemet.
    - S'il y a plusieurs fichiers copié, les paths collé sont délimité par des guillemets et séparer par un espace. C'est optimisé pour être collé dans une fenêtre d'ouverture de fichier.
    - **Variante +Maj (Ctrl+Maj+V) :**
      - Force les guillemets sur les paths copiés depuis l'expolateur, même s'il n'y a qu'un seul path de copié.
      - S'il y a plusieurs fichiers, une fenêtre vous demande quel(s) character(s) doivent être utilisé pour séparer les paths.
    - **Variante +Alt (Ctrl+Alt+V) :**
      - Force le collage par defaut, sans comportement particulier
  - Si le presse-papiers contient exactement une URL :
    - Un lien hypertexte HTML sera généré avant le collage. Si votre logiciel supporte le HTML5 (ex : Discord, Teams...) ou supporte la mise en page (ex : Microsoft Office suite ...), un lien spécial "HomePage/specificPage" sera collé. (Sinon, l'URL sera collée comme un texte habituel)
    - **Variation +Shift (Ctrl+Shift+V):**
      - L'URL sera collé comme un texte habituel
  - Si le clipboard contient du text :
    - Colle le texte du clipboard normalement, sans comportements particuliers.
    - **Variante +Maj (Ctrl+Maj+V) :**
      - Recherche dans le clipboard les paths Windows, et ne colle que les paths Windows.
      - Si le Clipboard ne contient pas de paths Windows, mais que le texte est copié avec une police spéciale, alors seul le texte brut est collé

---
## Win+Enter (+Alt/+Maj) :
Win+Enter a pour fonction pricipale d'executer des paths Windows. Que ce soit d'ouvrir le path dans l'explorateur windows, d'ouvrir le fihcier avec son logiciel par défaut, ou encore d'executer un path vers un executable et ses arguments.
### Sur une sélection de *texte* ou un *control Windows* :
*Comprenez "control Windows" comme une pop-up affichant un message d'erreur qui contient un path windows. Cela s'étend à n'importe quel objet d'un UI qui permet de faire un Ctrl+C pour réccupérer un message, comme faire un Ctrl+C sur une ligne d'erreur d'Atoll, ou de Visual Studio.*

  - Si le texte (*un message d'erreur, un warning ...*) contient un path vers un dossier/fichier/executabe :
    - On ouvre le fichier avec son logiciel par defaut, execute l'exe, ou ouvre une explorateur Windows au path sélectionné.
    ***=> Vous pouvez sélectionner votre path grossièrement ! <=
    => On détecte la présence du path dans n'importe quel texte <=***
  - Si plusieurs paths sont trouvés :
    - On vous demande quel path vous souhaitez ouvrir (un menu à choix multiple apparait).
  - **Variante +Alt (Win+Alt+Enter) :**
     - Ouvre le dossier parent du path sélectionné.
  - **Variante +Maj (Win+Maj+Enter) :**
    - Execute le texte sélectionné sans rechercher de path dans la selection de txt :
      *=> Vous devez vous appliquer sur la selection de texte pour cette variante <=*
    - Utile si vous executez un texte qui n'a pas de lien avec un path Windows comme :
      - Ouvrir un lien https.
      - Un .exe AVEC des parametres.
    - Utile si le path que vous voulez ouvrir est trop exotique pour être reconu (ex : il y a des virgules dans un nom de dossier ou fichier !)

### Ctrl+Win+Enter (+Alt/+Maj) :
*Similaire à Win+Enter, mais cette fois-ci, on réccupère les paths depuis le clipboard actuel.
Utile si le texte n'est pas réccupérable par un simple Ctr+C, comme les message d'erreur de Planet : il faut d'abord faire CliqueDroit > Copier pour réccupérer le message*
  - Si clipboard contient un path vers un dossier/fichier/executabe :
    - On ouvre le fichier avec son logiciel par defaut, execute l'exe, ou ouvre une explorateur Windows au path sélectionné.
    ***=> On détecte la présence du path dans n'importe quel texte du clipboard<=***
  - Si plusieurs paths sont trouvés :
    - On vous demande quel path vous souhaitez ouvrir (un menu à choix multiple apparait).
  - **Variante +Alt (Ctrl+Win+Alt+Enter) :**
     - Ouvre le dossier parent du path trouvé.
  - **Variante +Maj (Ctrl+Win+Maj+Enter) :**
    - Execute le texte contenu dans le clipboard sans rechercher de path dans la selection de txt :
     *=> Vous devez vous appliquer sur la selection de texte pour cette variante <=*
    - Utile si vous executez un texte qui n'a pas de lien avec un path Windows comme :
      - Ouvrir un lien https.
      - Un .exe AVEC des parametres.
    - Utile si le path que vous voulez ouvrir est trop exotique pour être reconu (ex : il y a des virgules dans un nom de dossier ou fichier !).

---
## Win+/ sur une sélection de texte :
  - Propose de remplacer les '/' par des '\\'.
  - Propose d'échapper les '\\' en '\\\\'. 
  - Propose de remplacer les '\\' et '\\\\' par des '/'.

---

## Win+F2 sur une sélection de fichiers de l'explorateur Windows :
 - Ajoute un préfixe aux noms des fichiers sélectionnés.

---

##  Win+ClickDroit sur n'importe quel fenêtre Windows :
Ouvre un menu à choix multiple :
  - Propose de mettre la fenetre toujours au premier plan
  - Propose d'ouvrir le dossier d'installation de l'application.
  - Propose de copier le chemin vers l'executable de la fenêtre.
  - Propose de copier le PID de la fenêtre.
  - Propose de Kill l'application pointé par la souris (une pop-up vous demande de valider l'action).

##  Win+Alt+Up sur n'importe quel fenêtre Windows :
Force la fenêtre active à toujours rester au dessu de toutes les autres.
Pour retirer cette propriété, relancez la macro Win+Alt+Up dessus

&nbsp;

---

## Compatibilité VirtualBox :

Il est possible de traduire les paths de la machine hôte vers la machine virtuelle à certaines conditions.
- **Necessite :**
    - Avoir installé l'"Additions Invitée/Guest Additions".
    - Avoir activé le press-papier bi-directionnel.
    - Avoir monté un dossier partagé pointant sur la racine de vos disques de la machine hôte.
      - Avoir laissé le nom par défaut du montage (pour que 'C:\' apparaisse dans la VM sous \\\\Vboxsvr\c_drive).
    - Avoir lancé la macro "ExplorateurTools.exe" dans la VM.
- **Alors, vous pouvez :**
    - Coller des fichiers Host --> VM directement (Ctrl+V uniquement, pas via le menu contextuel)
      -  La contraposée VM --> Host ne fonctionne pas (il n'est pas possible d'acceder à un fichier du disque de la VM  depuis la machine hôte =(
    - Si vous copiez un texte depuis la machine hôte --> VM, et qu'il contient des paths, ces dernier sont automatiquement traduit en path compatible avec la VM (quand le txt est collé dans la VM).
      - La contraposé VM --> Host est "***possible***" tant que les paths pointent un dossier partagé de la VM.
      - Très utile pour convertir des scripts appelant des fichiers sur la machine hôte : 
        - Ctrl+A, Ctrl+C dans le script de la machine hôte, Ctrl+V dans le script de la VM : Tous les paths issus de la machien hôte seront traduits en path compatible avec la VM.