/*
Test :
- on peut toujours coller une liste de fichier dans l'explorateur, même apre un collage de zones de texte
- on peut toujours coller un unique fichier dans l'explorateur, même apre un collage de zones de texte
- on peut toujours coller dans la barre d'addresse de l'explorateur
- on peut toujours coller les fichier dans une fenetre type "Open file"
- on peut coller la liste de fichier dans l'edit de selection de fichier dans une fenetre type "Open"
- on peut coller le path d'un unique ficher dans des zones de texte
- on peut coller une liste de fichiers dans des zones de texte
- on peut personalider le delimiter de la liste de fichier de coller avec ctrl+maj+V : 
    -> le delimiter est sauvegardé et utilisé par défaut dans les prochains collages
- on peut copier une portion d'image sans probleme (une selction de paint par ex)
- coller une url dans un logiciel qui prend en compte les mises en pages ou l'HTML5 (Microsoft Office, Discord...) colle un lien hypertext personalié
    -> Ctrl+Maj+V colle le lien original

- ça fonctionne aussi sur le bureau
- ça fonctionne vers une VM

*/

; decoupe un URL entre sa racine, son chemin et ses parametres
global regExSplitHTML := '^https?:\/\/(?:www\.)?([-a-zA-Z0-9@:%._\+~#=&;%]{2,256}\.[a-z]{2,6}\b)*(?::\d+)?(\/[\/\d\w\.%&#;-]*)*(?:[\?])*(\S+)*$'
/* ex: https://www.asd.google.com/api/login.php?q=some+text&param=3#dfsdf
==> 1 = asd.google.com
==> 2 = /api/login.php
==> 3 = q=some+text&param=3#dfsdf
*/

CtrlV()
{
  useShift := GetKeyState("Shift")
  useCtrl  := true
  useAlt   := GetKeyState("Alt")

  isFileCopied       := WClip.HasFormat(WClip.ClipboardFormats.CF_HDROP)
  isTXTCopied        := WClip.HasFormat(WClip.ClipboardFormats.CF_TEXT)
  isImageCopied      := WClip.HasFormat(WClip.ClipboardFormats.CF_BITMAP)
  isPolicedTxtCopied := WClip.GetHtml() != "" or WClip.GetRTF() != ""
  

  ; ------------------------  Si on ne sais pas ce que c'est, on laisse le fonctionnement par defaut ---------------------------


  if !(isFileCopied or isTXTCopied or isImageCopied)
    goto defaultPast

  ; ------------------------  Si le clipboard contient exactement un lien HTML, on en fait un lien hypertext --------------------

  if not useShift and isTXTCopied and !isPolicedTxtCopied and RegExMatch(WClip.GetText(), regExSplitHTML, &splitURL)
  {
    ; Sorts according to the string lengths, longer first
    strLenghtSortDesc(s1, s2, *)
    {
      s1 := StrLen(s1), s2 := StrLen(s2)
      return s1 > s2 ? -1 : s1 < s2 ? 1 : 0  
    }
    urlRoot := StrSplit(Sort(splitURL[1],"D.", strLenghtSortDesc), '.')[1]

    arrFold := StrSplit(splitURL[2], "/")
    id := arrFold.Length

    while id > 0
    {
      urlDecoration := StrReplace(uriDecode(arrFold[id]), "_", Chr(0x00A0))

      /* wee skip any title : shorter than 3 character
                              or have one digit != {2,4} or more than 1 digit inside a word
                              or begining by a digit
                              or does not contain any word character */
      if RegExMatch(urlDecoration, "i)^.{1,3}$|[0135-9]\d*[^\d\n]\N*$|^\d.*|^\W*$")
        --id
      else
        break
    }

    if id = 0
      hypertext := '<a href="' splitURL[0] '">' urlRoot '</a>'
    else
      hypertext := '<a href="' splitURL[0] '">' urlRoot '/' urlDecoration '</a>'

    WClip.SetHTML(hypertext)
    WClip._waitClipReady()
  }

  ; ------------------------  Si on est dans une fenetre type explorateur windows -----------------------------------------------
  currentContHWND := ControlGetFocus(A)
  if currentContHWND
    currentCont := ControlGetClassNN(currentContHWND)
  else
    currentCont := ""

  currentClass := WinGetClass(A)

  mainControlIsExplorateur := InStr(currentCont, "DirectUIHWND") or InStr(currentCont, "SysListView")
  if InStr(currentCont, "DirectUIHWND") ; il y a un risque que DirectUIHWND soit enfaite le control de la Recherche !
  {
    ControlGetPos &X, &Y, &Width, &Height, currentCont, A
    mainControlIsExplorateur := Height > 75 ; il  semblerai que le control de type Explorateur ne peut pas être moins haut que 125px, et le control de Recherche ne fasse que 24px de haut
  }

  windowsIsResizble := WinGetStyle(A) & 0x40000
    
;MsgBox 'mainControlIsExplorateur ' mainControlIsExplorateur
  if mainControlIsExplorateur
  {
    ; Si le presse papier contient des fichiers, on laisse le comportement par défaut. 
    if isFileCopied
      goto defaultPast

    canFindCurrentPath := ( ! InStr(currentCont, "SysListView") or currentCont == "SysListView321") ; and mainControlIsExplorateur
    
    ; Si on sait qu'on ne poura pas connaitre la position de l'explorateur, on laisse le comportement par defaut
    if !canFindCurrentPath
      goto defaultPast
    
    ; On réccupère la position de l'explorateur
    outPath := ""
    if InStr(currentCont, "DirectUIHWND")
      outPath := GetActiveExplorerPath()
    else if currentCont == "SysListView321"
      outPath := A_Desktop

    
    if FileExist(outPath) ; Si on a pu reccuperer notre position, et qu'on ne colle pas une liste de fichier
    {
      if useShift ; if Ctrl+Shift+V on explorer control, we create file
      {
        if isTXTCopied
          createFile(outPath, A_Clipboard)		
        else if isImageCopied
          createFileImageFromClipboard(outPath)
        return
      }
      else
      {
        if FileExist(clipUniquePath := Trim(A_Clipboard, "`"' `n`t`r"))
          wClip.iSetFiles(clipUniquePath)
        else if strListPath := StrJoin(findValidPathsInStr_arr(A_Clipboard), '`n')
          wClip.iSetFiles(strListPath)
        else if isImageCopied ;  If we past an image without "shift" we advise user in the case he forgot the right hotkey to create file and not shift
          {
            toolTipAutoClose 'Ctrl + Shift + V to past a picture from clipboard'  
            goto defaultPast
          }
        else 
        {
          toolTipAutoClose 'No valid path can be found.`nCtrl + Shift + V to creat a txt file from clipboard'
          goto defaultPast
        }
        
        wClip.iPaste()
        WClip.iClear()
        return
      }      
    }
    Else	; si on ne peut pas trouver le path de la fenêtre windows, on ne fait rien
      goto defaultPast
        
  }
  
  ;------------------------- Si on est dans la bare URL de l'explorateur, on enleve le fileName -----------------------
  else if (currentClass == "CabinetWClass" and (currentCont == "Edit1" or currentCont == "Microsoft.UI.Content.DesktopChildSiteBridge2"))
    or (currentClass == "#32770" and currentCont == "Edit2" and windowsIsResizble) ; window's file property is also #32770, but is not resizable.
  {
    pathToPast := A_Clipboard
    typeFile := FileExist(pathToPast)

    if typeFile == "" ; Si le clipboard ne contient pas un path valide, on essaie de le corriger, de l'extraire
    {
      arrFilePath := findPathsInStr_arr(pathToPast)
      ;MsgBox arrFilePath.Count()
      if arrFilePath.Length == 0
        Goto defaultPast

      if arrFilePath.Length > 1
      {
        pathToPast := askMenuList(arrFilePath)
        ;MsgBox dirName
        if ! pathToPast
          return

        Send '{F4}^a'
        sleep 300
      }
      Else
        pathToPast := arrFilePath[1]

      typeFile := FileExist(pathToPast)
    }
  
    while(!InStr(typeFile, "D") and pathToPast)
    {
      SplitPath pathToPast, , &pathToPast, &OutExtension
      typeFile := FileExist(pathToPast)
    }

    if pathToPast = ""
      pathToPast := A_Clipboard

    if VerCompare(A_OSVersion, "10.0.22000") > 0 ; Win11
    {
      WClip.Paste(pathToPast)
    }
    else{
      EditPaste pathToPast, currentCont, A
    }
  }

  ;------------------------- Si on est dans une fenetre d'ouverture de fichier (ahk_class #32770) et qu'on est sur l'Edit du/des nom(s) de fichier(s), on utilise "ControlSetText" à la place du Ctrl+V pour ne pas etre limité à 256 characters -----------------------
  else if currentClass == "#32770" and currentCont == "Edit1" ; La fenetre de recherche globale de fichier d'Eclipse utilise aussi cette classe et possede un Edit1 ='(
  {
    if isTXTCopied
    {
      pathToPast := Trim(A_Clipboard, ' `'`"`t`r`n')
      typeFile := FileExist(pathToPast)
      if typeFile == "" ; Si le clipboard ne contient pas un path valide, on essaie de le corriger, de l'extraire
      {
        arrFilePath := findValidPathsInStr_arr(pathToPast)
        if(arrFilePath.Length = 0)
          goto defaultPast

        pathToPast := StrJoin(arrFilePath, '" "', '"', '"')
      }
      else
        pathToPast := '"' pathToPast '"'
    }
    else if isFileCopied {
      pathToPast := A_Clipboard
      pathToPast := strReplace(pathToPast, '`r')
      pathToPast := '"' strReplace(pathToPast, '`n', '" "') '"'
    }
    else 
      goto defaultPast
    
    ControlSetText pathToPast, currentCont, A
  }
  ;------------------------- On est probablement dans une zone de texte        -----------------------
  ;------------------------  Si on veux détailler la liste des fichier copier -----------------------
  else if isFileCopied
  {
    if useAlt
      goto defaultPast

    clipFiles := StrReplace(A_Clipboard, "`r")

    nbLine := 0
    Loop Parse clipFiles, '`n'
      ++nbLine
    
    if nbLine = 0
      goto defaultPast
    else if nbLine = 1 and useShift
      clipFiles := '"' clipFiles '"'
    else if nbLine > 1
    {
      if(useShift)
      {
        delimiterRes := askForPathDelimiter()
        if delimiterRes.Result = "Cancel"
          return
        delimiter := delimiterRes.value
      }
      else
      {
        delimiter := iniLoad("defaultPathDelimiter", A_Space)

        delimiter := StrReplace(delimiter, '\t', '`t')
        delimiter := StrReplace(delimiter, '\n', '`n')
        delimiter := StrReplace(delimiter, '\r', '`r')
      }
      clipFiles := '"' StrReplace(clipFiles, "`n", '"' delimiter '"') '"'
    }

    WClip.Paste(clipFiles)
  }
  else if isTXTCopied and useShift
  {
    ; We extract paths OR we past only plain text, without HTML
    toPast := ""
    arrPath := findPathsInStr_arr(WClip.GetText())

    local whatToPast := 0 ; defaultPast

    if arrPath.Length > 0      and not isPolicedTxtCopied
      whatToPast := 1 ; past path(s)
    else if arrPath.Length = 0 and isPolicedTxtCopied
      whatToPast := 2 ; past without HTML
    else if arrPath.Length > 0 and isPolicedTxtCopied
        whatToPast := askMenuList([
          ["&V Past only windows paths", 1],
          ["&A Past all in plain txt", 2]
        ], 0)
        

    switch whatToPast {
      case 0:
        Goto defaultPast
      case 1: ; past path(s)
        if arrPath.Length = 1
          toPast := arrPath[1]
        else
        {
          delimiter := askForPathDelimiter()
          if delimiter.Result = "Cancel"
            return
  
          toPast := StrJoin(arrPath, '"' delimiter.value '"', '"',  '"')
          ;MsgBox  dirName
        }
      case 2: ; past all without HTML
        toPast := A_Clipboard
        ; toPast := WClip.GetText()
      default:
        Goto defaultPast
    }
    
    WClip.Paste(toPast)
  }
  else
    goto defaultPast

  return
  
  defaultPast:
    keyModifier   := '^'
    if useShift
      keyModifier .= '+'
    if useAlt
      keyModifier .= '!'
      
    send keyModifier 'v'
  Return
}

GetActiveExplorerPath()
{
  explorerHwnd := WinGetID(A)
  if (explorerHwnd)
  {
    for window in ComObject("Shell.Application").Windows
    {
      if (window.hwnd==explorerHwnd)
      {
        return window.Document.Folder.Self.Path
      }
    }
  }
  ; si on est ici, c'est qu'on n'a pas pu trouver le path de manière classique
  Directorie := ControlGetText("ToolbarWindow323", A)
  Directorie := RegExReplace(Directorie, "[^:]+: ",,,1)
  if FileExist(Directorie)
    return Directorie
  
  Send '{F4}'
  sleep 30
  Directorie := ControlGetText("Edit1", A)
  
  if FileExist(Directorie)
    return Directorie
  
  Directorie := ControlGetText('Edit2', A)
  
  if FileExist(Directorie)
    return Directorie
    
    
    return ""
}


createFile(dir, data)
{
  loop parse, data, '`t ,;:/\?"<>|`n`r*.'
  {
    fileName := A_LoopField
    if InStr(fileName, "---")
      Continue
      
    if fileName != ""
      Break
  }
  
  fileName .= '.txt'
  filePath := dir '\' fileName

  askAgain := false
  filePath := FileSelect('S16', filePath, 'Create File from clipboard')

  if not filePath
    return      
  
  if FileExist(filePath)
    FileDelete filePath

  FileAppend data, filePath
}

askForPathDelimiter(nbPath := "More than one ")
{
  local msg := nbPath "
  (
paths has been detected, choose a delimiter (ex : a space character, comma, \t, \n, \r\n ...)
\t : Tabulation
\n : Line return
\r : Return carriage
)"

  local delimiter := iniLoad("defaultPathDelimiter", A_Space)  
  delimiter := InputBox(msg, 'Wich character between paths ?',"H200", delimiter)
     
  if delimiter.Result = "Cancel"
    return delimiter

  iniSave "defaultPathDelimiter", delimiter.value

  delimiter.value := StrReplace(delimiter.value, '\t', '`t')
  delimiter.value := StrReplace(delimiter.value, '\n', '`n')
  delimiter.value := StrReplace(delimiter.value, '\r', '`r')

  return delimiter
}
