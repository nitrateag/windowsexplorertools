
WinEnter()
{
  useShift := GetKeyState("Shift")
  useCtrl  := GetKeyState("Ctrl")
  useAlt   := GetKeyState("Alt")

  if ! useCtrl
  {
      ClipSaved := ClipboardAll()
      Send '^c'
      sleep 200
  }
  
  clipStr := Trim(A_Clipboard, "`n`r `t")

  if !FileExist(clipStr) and ! useShift
  {
    arrPath := findPathsInStr_arr(clipStr)

    if arrPath.Length > 1
    {
      matChoice := []
      for path in arrPath
        matChoice.Push([path "`tOpen", path])

      selection := askMenuList(matChoice)

      if selection = ""
        Return

      clipStr := selection
    }
    Else if arrPath.Length == 1
        clipStr := arrPath[1]

    if !FileExist(clipStr)
    {
      strValid := clipStr
      While(strValid && !FileExist(strValid))
      {
        SplitPath strValid,, &strValid
      }
      
      rep := MsgBox(clipStr "`nDoes not seem to exist. Do you want to open the first valid parent path?`n`n" strValid "`n`nNo = I know what I am doing, open it anyway!", clipStr, "YesNoCancel")
      If rep = "Yes"
        clipStr := strValid
      else if rep = "Cancel"
        return
    }
  }
  else if !useShift
  {
    clipStr := strReplace(clipStr, "/", "\")
  }

  if clipStr != ""
  {
    if useAlt and FileExist(clipStr) 
      Run 'explorer.exe /separate, /select, "' Trim(clipStr, '"`'') '"'
    else
      run clipStr
  }
    
  if ! useCtrl
  {
      A_Clipboard := ClipSaved
      ClipSaved := "" ; Free the memory in case the clipboard was very large.
  }
}
